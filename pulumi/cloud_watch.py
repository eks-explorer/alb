from pulumi_eks import Cluster
import pulumi_aws as aws
import pulumi_kubernetes as k8s
import pulumi
from cloudwatch_components.cloud_watch_agent import CloudWatchAgent
from cloudwatch_components.fluent_bit_agent import FluentBitAgent

def cloud_watch(cluster: Cluster, opts):
    # Attach the cloudwatch policy to the cluster
    cloudwatch_policy_attachment = aws.iam.PolicyAttachment("cloudwatch-policy-attachment",
        policy_arn="arn:aws:iam::aws:policy/CloudWatchAgentServerPolicy",
        roles=[cluster.instance_roles.apply(lambda r: r[0].name)])

    amazon_cloudwatch_namespace = k8s.core.v1.Namespace("amazon_cloudwatchNamespace",
        api_version="v1",
        kind="Namespace",
        metadata=k8s.meta.v1.ObjectMetaArgs(
            name="amazon-cloudwatch",
            labels={
                "name": "amazon-cloudwatch",
            },
        ),
        opts=opts)

    CloudWatchAgent(cluster=cluster, opts=opts)

    FluentBitAgent(cluster=cluster, opts=opts)
    