
Write-Host "Remove finalizer on ingress keycloak"
kubectl patch ingress keycloak -n keycloak -p '{"metadata":{"finalizers":[]}}' --type=merge

$tgb_name=$(kubectl get targetgroupbinding -n keycloak -o name).split('/')[1]

if ($tgb_name) {
    Write-Host "Remove finalizer on targetgroupbinding keycloak"
    Write-Host "kubectl patch targetgroupbinding $tgb_name -n keycloak -p '{"metadata":{"finalizers":[]}}' --type=merge"
    kubectl patch targetgroupbinding $tgb_name -n keycloak -p '{"metadata":{"finalizers":[]}}' --type=merge

    Write-Host "Delete the target group binding"
    Write-Host "kubectl delete targetgroupbinding $tgb_name -n keycloak"
    kubectl delete targetgroupbinding $tgb_name -n keycloak
} else {
    Write-Host "Skip targetgroupbinding deletion as it is not found"
}

Write-Host "Remove finalizer on namespace keycloak"
kubectl patch namespace keycloak -p '{"metadata":{"finalizers":[]}}' --type=merge

pulumi destroy -y

Write-Host "Delete load balancer"
$lb = $(aws elbv2 describe-load-balancers | ConvertFrom-Json)[0].LoadBalancers
$lbArn = ($lb | Where-Object { $_.DNSName.StartsWith("k8s-eksalbexemplealbg") }).LoadBalancerArn
aws elbv2 delete-load-balancer --load-balancer-arn $lbArn

Write-Host "Delete vpc"
$vpcInfo = $(aws ec2 --output text --query 'Vpcs[*].{VpcId:VpcId,Name:Tags[?Key==`Name`].Value|[0],CidrBlock:CidrBlock}' describe-vpcs | Select-String eks-alb-pulumi-vpc)
$id = $vpcInfo.ToString().Split('vpc')
$vpcId = $id[2]
aws ec2 delete-vpc --vpc-id "vpc$vpcId"

pulumi refresh -y

pulumi destroy -y

Write-Host "pulumi config set enableWaf false"
pulumi config set enableWaf false
