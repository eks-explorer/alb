Write-Output "Set enableWaf false"
pulumi config set enableWaf false

pulumi up -y

aws eks update-kubeconfig --name eks-alb-pulumi

Write-Output "Set enableWaf true"
pulumi config set enableWaf true

pulumi up -y