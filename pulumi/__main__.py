from pulumi_kubernetes.networking.v1 import *
from apps.erabliere_api import erabliere_api
from apps.keycloak import keycloak
from apps.aws_alb import aws_alb
from cloud_watch import cloud_watch
from cluster import deploy_cluster_in_vpc
from apps.external_dns import external_dns
from apps.prometheus import prometheus
from waf import setup_WAF
import pulumi

config = pulumi.Config()

hosted_zone = "eks-explorer.freddycoder.com"
hosted_zone_id = "Z0625853ZC3AUZX1QZSB"

cluster, vpc, provider = deploy_cluster_in_vpc()

# Deploy the alb ingress controller to the cluster.
certificate, alb_helm_release = aws_alb(cluster=cluster, 
                      provider=provider, 
                      hosted_zone=hosted_zone, 
                      hosted_zone_id=hosted_zone_id)

# Route53 configuration
external_dns_release = external_dns(cluster=cluster, 
             provider=provider, 
             hosted_zone=hosted_zone, 
             hosted_zone_id=hosted_zone_id)

# Deploy prometheus to get metrics from the cluster
prometheus(provider=provider)

# Cloud watch
cloud_watch(cluster=cluster, opts=pulumi.ResourceOptions(provider=provider))

# Deploy erabliereapi app
erabliere_api(vpc=vpc, 
              provider=provider, 
              hosted_zone=hosted_zone, 
              certificate=certificate,
              depends_on=[external_dns_release])

# Deploy keycloak app
keycloak(vpc=vpc, 
         provider=provider, 
         hosted_zone=hosted_zone, 
         certificate=certificate,
         depends_on=[external_dns_release])

# WAF
if config.require('enableWaf') == "true":
    setup_WAF()