import pulumi_kubernetes as k8s
from pulumi_kubernetes.networking.v1 import *
import pulumi

# deploy prometheus using pulumi Release
def prometheus(provider):
    # Create a prometheus namespace
    prometheus_namespace = k8s.core.v1.Namespace("prometheus",
        metadata={
            "name": "prometheus",
        },
        opts=pulumi.ResourceOptions(provider=provider)
    )

    # Create a prometheus release
    prometheus_release = k8s.helm.v3.Release("prometheus",
        chart="prometheus",
        namespace=prometheus_namespace.metadata["name"],
        repository_opts=k8s.helm.v3.RepositoryOptsArgs(
            repo="https://prometheus-community.github.io/helm-charts",
        ),
        values={
            "alertmanager": {
                "enabled": False,
            },
            "grafana": {
                "enabled": False,
            },
            "server": {
                "persistentVolume": {
                    "enabled": False,
                },
            },
        },
        opts=pulumi.ResourceOptions(provider=provider)
    )