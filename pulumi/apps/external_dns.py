import pulumi_aws as aws
import pulumi
import pulumi_kubernetes as k8s

def external_dns(cluster, provider, hosted_zone, hosted_zone_id):
    # Create the new policy to allow changes to Route53
    route53_policy = aws.iam.Policy("route53-policy",
        description="Allow changes to Route53",
        policy={
            "Version": "2012-10-17",
            "Statement": [{
                "Effect": "Allow",
                "Action": [
                    "route53:ChangeResourceRecordSets",
                ],
                "Resource": [
                    "arn:aws:route53:::hostedzone/*",
                ],
            }, {
                "Effect": "Allow",
                "Action": [
                    "route53:ListHostedZones",
                    "route53:ListResourceRecordSets",
                ],
                "Resource": ["*"],
            }],
        })

    # Attach the policy to the EKS rule
    route53_policy_attachment = aws.iam.PolicyAttachment("route53-policy-attachment",
        policy_arn=route53_policy.arn,
        roles=[cluster.instance_roles.apply(lambda r: r[0].name)])

    # Create a namespace for external-dns
    external_dns_namespace = k8s.core.v1.Namespace("external-dns-namespace",
        metadata={
            "name": "external-dns",
        },
        opts=pulumi.ResourceOptions(provider=provider))

    # Instaling external-dnsusing pulumi Release function
    external_dns_release = k8s.helm.v3.Release("external-dns",
        chart="external-dns",
        namespace="external-dns",
        repository_opts=k8s.helm.v3.RepositoryOptsArgs(
            repo="https://charts.bitnami.com/bitnami",
        ),
        values={
            "provider": "aws",
            "domainFilters": [hosted_zone],
            "policy": "sync",
            "registry": "txt",
            "txtOwnerId": hosted_zone_id,
            "interval": "3m",
            "rbac": {
                "create": True,
                "serviceAccountName": "external-dns",
                "serviceAccountAnnotations": {
                    "eks.amazonaws.com/role-arn": cluster.instance_roles.apply(lambda r: r[0].arn),
                },
            },
        },
        opts=pulumi.ResourceOptions(provider=provider))
    
    return external_dns_release

