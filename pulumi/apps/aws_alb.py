import alb_iam_policy
import pulumi_aws as aws
import pulumi_kubernetes as k8s
import json
import pulumi
from pulumi_eks import Cluster

def aws_alb(cluster: Cluster, provider, hosted_zone: str, hosted_zone_id: str) -> aws.acm.Certificate:
    oidc_arn = cluster.core.oidc_provider.arn
    oidc_url = cluster.core.oidc_provider.url

    alb_ing_ns = "aws-lb-controller"
    service_account_name = f"system:serviceaccount:{alb_ing_ns}:aws-load-balancer-controller"

    iam_role = aws.iam.Role("alb-loadbalancer-controller-role",
    name="alb-loadbalancer-controller-role",
    assume_role_policy=pulumi.Output.all(oidc_arn, oidc_url).apply(lambda args: json.dumps({
        "Version": "2012-10-17",
        "Statement": [{
            "Effect": "Allow",
            "Principal": {
                "Federated": args[0],
            },
            "Action": "sts:AssumeRoleWithWebIdentity",
            "Condition": {
                "StringEquals": {
                    f"{args[1]}:sub": service_account_name,
                },
            },
        }],
    })))

    iam_role_policy = aws.iam.Policy("alb-loadbalancer-controller-policy",
    name="alb-loadbalancer-controller-policy",
    policy=alb_iam_policy.alb_ingress_policy,
    opts=pulumi.ResourceOptions(parent=iam_role))

    policy_attachment = aws.iam.PolicyAttachment("alb-loadbalancer-controller-policy-attachment",
    name="alb-loadbalancer-controller-policy-attachment",
    policy_arn=iam_role_policy.arn,
    roles=[iam_role.name],
    opts=pulumi.ResourceOptions(parent=iam_role))

    # Create a namespace for the ALB Ingress Controller.
    alb_namespace = k8s.core.v1.Namespace(alb_ing_ns,
    metadata={
        "name": alb_ing_ns,
    },
    opts=pulumi.ResourceOptions(provider=provider))

    service_account = k8s.core.v1.ServiceAccount("aws-lb-controller-sa",
    metadata={
        "name": "aws-load-balancer-controller",
        "namespace": alb_namespace.metadata["name"],
        "annotations": {
            "eks.amazonaws.com/role-arn": iam_role.arn,
        },
    },
    opts=pulumi.ResourceOptions(provider=provider, parent=provider))

    # Configure the ALB Ingress Controller.
    alb_helm_release = k8s.helm.v3.Release("alb-ingress-controller",
        k8s.helm.v3.ReleaseArgs(
            name="alb-ingress-controllers",
            chart="aws-load-balancer-controller",
            namespace=alb_namespace.metadata["name"],
            repository_opts=k8s.helm.v3.RepositoryOptsArgs(
                repo="https://aws.github.io/eks-charts",
            ),
            values={
                "region": "ca-central-1",
                "clusterName": cluster.core.cluster.name,
                "vpcId": cluster.vpc_id,
                "serviceAccount": {
                    "create": False,
                    "name": service_account.metadata["name"],
                },
                "podLabels": {
                    "stack": pulumi.get_stack(),
                    "app": "aws-lb-controller",
                },
            },
        ),
        opts=pulumi.ResourceOptions(provider=provider, parent=alb_namespace))

    # Deploy a certificate to use for the ALB Ingress Controller.
    alb_cert = aws.acm.Certificate("alb-ingress-cert",
        domain_name="*." + hosted_zone,
        tags={
            'Environment': 'dev',
        },
        validation_method="DNS",
    )

    # Create a DNS record to prove that we _own_ the domain we're requesting a certificate for.
    alb_cert_validation = aws.route53.Record("alb-ingress-cert-validation",
        name=alb_cert.domain_validation_options[0].resource_record_name,
        records=[alb_cert.domain_validation_options[0].resource_record_value],
        ttl=300,
        type=alb_cert.domain_validation_options[0].resource_record_type,
        zone_id=hosted_zone_id,
    )

    return alb_cert, alb_helm_release