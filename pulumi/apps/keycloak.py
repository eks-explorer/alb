import pulumi_kubernetes as k8s
import pulumi_aws as aws
from pulumi_kubernetes.networking.v1 import *
import pulumi

# Deploy keycloak using pulumi Release and keycloak bitnami chart
def keycloak(vpc, provider, hosted_zone: str, certificate: aws.acm.Certificate, depends_on, namespaceName: str = "keycloak"):
    # Create a keycloak namespace
    keycloak_namespace = k8s.core.v1.Namespace(namespaceName,
        metadata={
            "name": namespaceName,
        }, 
        opts=pulumi.ResourceOptions(provider=provider)
    )

    # Deploy a postgresql database for keycloak
    postgresql_deployment = k8s.apps.v1.Deployment(f"{namespaceName}-postgresql-deployment",
        metadata={
            "namespace": keycloak_namespace.metadata["name"],
            "name": "postgresql-deployment",
        },
        spec={
            "replicas": 1,
            "selector": {
                "match_labels": {
                    "app.kubernetes.io/name": "postgresql-app",
                },
            },
            "template": {
                "metadata": {
                    "labels": {
                        "app.kubernetes.io/name": "postgresql-app",
                    },
                },
                "spec": {
                    "containers": [{
                        "name": "postgresql-keycloak",
                        "image": "postgres",
                        "ports": [{
                            "containerPort": 5432,
                        }],
                        "env": [{
                            "name": "POSTGRES_USER",
                            "value": "keycloak",
                        }, {
                            "name": "POSTGRES_PASSWORD",
                            "value": "keycloak",
                        }, {
                            "name": "POSTGRES_DB",
                            "value": "keycloak",
                        }]
                    }],
                },
            },
        },
        opts=pulumi.ResourceOptions(provider=provider))

    # Create a NodePort service for postgresql
    postgresql_service = k8s.core.v1.Service(f"{namespaceName}-postgresql-service",
        metadata={
            "namespace": keycloak_namespace.metadata["name"],
            "name": "postgresql-service",
        },
        spec={
            "type": "NodePort",
            "ports": [{
                "port": 5432,
                "targetPort": 5432,
            }],
            "selector": {
                "app.kubernetes.io/name": "postgresql-app",
            },
        },
        opts=pulumi.ResourceOptions(provider=provider))
    
    # Keycloak configmap to restrict admin access
    kc_admin_config_map = { 
        "apiVersion": "v1", 
        "kind": "ConfigMap", 
        "metadata": { 
            "name": "admin-ip-ranges", 
            "namespace": keycloak_namespace.metadata["name"]
        }, 
        "data": { 
            "allowlist": "74.50.178.224"
        } 
    }
    
    k8s.core.v1.ConfigMap( 
        f"{namespaceName}-admin-ip-ranges", 
        metadata=kc_admin_config_map['metadata'], 
        data=kc_admin_config_map['data'], 
        opts=pulumi.ResourceOptions(provider=provider))

    # Create a keycloak release
    keycloak_release = k8s.helm.v3.Release(f"{namespaceName}-keycloak",
        chart="keycloak",
        namespace=keycloak_namespace.metadata["name"],
        repository_opts=k8s.helm.v3.RepositoryOptsArgs(
            repo="https://charts.bitnami.com/bitnami",
        ),
        values={
            "fullnameOverride": "keycloak",
            "production": True,
            "replicaCount": 2,
            "ingress": {
                "enabled": True,
                'ingressClassName': 'alb',
                "hostname": f"{namespaceName}.{hosted_zone}",
                "pathType": "Prefix",
                'path': '/',
                "annotations": {
                    "alb.ingress.kubernetes.io/group.name": f"{pulumi.get_stack()}-alb-group",
                    "alb.ingress.kubernetes.io/scheme": "internet-facing",
                    "alb.ingress.kubernetes.io/target-type": "ip",
                    "alb.ingress.kubernetes.io/subnets": vpc.public_subnet_ids.apply(lambda ids: ",".join(ids)),

                    # Setup HTTPS
                    "alb.ingress.kubernetes.io/listen-ports": '[{"HTTP": 80}, {"HTTPS": 443}]',
                    "alb.ingress.kubernetes.io/certificate-arn": certificate.arn,
                    "alb.ingress.kubernetes.io/ssl-redirect": '443',         
                },
            },
            "proxy": "edge",
            "auth": {
                "adminUser": "admin",
                "adminPassword": "admin",
            },
            "service": {
                "type": "NodePort",
            },
            'existingConfigMap': 'admin-ip-range',
            "postgresql": {
                "enabled": False,
            },
            "externalDatabase": {
                "host": postgresql_service.metadata["name"],
                "database": "keycloak",
                "user": "keycloak",
                "password": "keycloak",
            },
            "metrics": {
                "enabled": True
            }
        },
        opts=pulumi.ResourceOptions(provider=provider, depends_on=depends_on + [postgresql_deployment])
    )
