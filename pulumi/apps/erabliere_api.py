import pulumi_kubernetes as k8s
import pulumi_aws as aws
from pulumi_kubernetes.networking.v1 import *
import pulumi

def erabliere_api(vpc, provider, hosted_zone: str, certificate: aws.acm.Certificate, depends_on):
    # Deploy erabliereapi/erabliereapi to the cluster using Deployment and Service in the namespace erabliereapi.
    erabliereapi_namespace = k8s.core.v1.Namespace("erabliereapi",
        metadata={
            "name": "erabliereapi",
        }, 
        opts=pulumi.ResourceOptions(provider=provider)
    )

    erabliereapi_deployment = k8s.apps.v1.Deployment("erabliereapi-deployment",
        metadata={
            "namespace": erabliereapi_namespace.metadata["name"],
            "name": "erabliereapi-deployment",
        },
        spec={
            "replicas": 1,
            "selector": {
                "match_labels": {
                    "app.kubernetes.io/name": "erabliereapi-app",
                },
            },
            "template": {
                "metadata": {
                    "labels": {
                        "app.kubernetes.io/name": "erabliereapi-app",
                    },
                },
                "spec": {
                    "containers": [{
                        "name": "erabliereapi-app",
                        "image": "erabliereapi/erabliereapi:v3-dev",
                        "ports": [{
                            "containerPort": 80,
                        }],
                        "env": [{
                            "name": "ASPNETCORE_ENVIRONMENT",
                            "value": "Production",
                        }]
                    }],
                },
            },
        },
        opts=pulumi.ResourceOptions(provider=provider))

    # Create a NodePort service for erabliereapi.
    erabliere_api_service = k8s.core.v1.Service("erabliereapi-service",
        metadata={
            "namespace": erabliereapi_namespace.metadata["name"],
            "name": "erabliereapi-service",
        },
        spec={
            "type": "NodePort",
            "ports": [{
                "port": 80,
                "target_port": 80,
                "protocol": "TCP",
            }],
            "selector": {
                "app.kubernetes.io/name": erabliereapi_deployment.spec["selector"]["match_labels"]["app.kubernetes.io/name"],
            },
        },
        opts=pulumi.ResourceOptions(provider=provider, depends_on=depends_on))

    # Deploy the product api in the erabliereapi namespace.
    productapi_deployment = k8s.apps.v1.Deployment("productapi-deployment",
        metadata={
            "namespace": erabliereapi_namespace.metadata["name"],
            "name": "productapi-deployment",
        },
        spec={
            "replicas": 1,
            "selector": {
                "match_labels": {
                    "app.kubernetes.io/name": "productapi-app",
                },
            },
            "template": {
                "metadata": {
                    "labels": {
                        "app.kubernetes.io/name": "productapi-app",
                    },
                },
                "spec": {
                    "containers": [{
                        "name": "productapi-app",
                        "image": "erabliereapi/product-api:latest",
                        "ports": [{
                            "containerPort": 3000,
                        }],
                        "env": [{
                            'name': 'apiSettings__prefix',
                            'value': '/product-api',
                        },
                        {
                            'name': 'logger__level',
                            'value': 'info',
                        }],
                    }],
                },
            },
        },
        opts=pulumi.ResourceOptions(provider=provider))

    # Create a NodePort service for productapi.
    productapi_service = k8s.core.v1.Service("productapi-service",
        metadata={
            "namespace": erabliereapi_namespace.metadata["name"],
            "name": "productapi-service",
        },
        spec={
            "type": "NodePort",
            "ports": [{
                "port": 80,
                "target_port": 3000,
                "protocol": "TCP",
            }],
            "selector": {
                "app.kubernetes.io/name": productapi_deployment.spec["selector"]["match_labels"]["app.kubernetes.io/name"],
            },
        },
        opts=pulumi.ResourceOptions(provider=provider, depends_on=depends_on))

    # Create the ingress for erabliereapi and productapi.
    erabliereapi_ingress = k8s.networking.v1.Ingress("erabliereapi-ingress",
        metadata={
            "namespace": erabliereapi_namespace.metadata["name"],
            "name": "erabliereapi-ingress",
            "annotations": {
                "alb.ingress.kubernetes.io/group.name": f"{pulumi.get_stack()}-alb-group",
                "alb.ingress.kubernetes.io/scheme": "internet-facing",
                "alb.ingress.kubernetes.io/target-type": "ip",
                "alb.ingress.kubernetes.io/subnets": vpc.public_subnet_ids.apply(lambda ids: ",".join(ids)),

                # Setup HTTPS
                "alb.ingress.kubernetes.io/certificate-arn": certificate.arn,
                "alb.ingress.kubernetes.io/listen-ports": '[{"HTTP": 80}, {"HTTPS": 443}]',
                "alb.ingress.kubernetes.io/ssl-redirect": '443',
            },
        },
        spec=IngressSpecArgs(
            ingress_class_name="alb",
            rules=[IngressRuleArgs(
                host=f"erabliereapi.{hosted_zone}",
                http=HTTPIngressRuleValueArgs(
                    paths=[HTTPIngressPathArgs(
                        backend=IngressBackendArgs(
                            service=IngressServiceBackendArgs(
                                name=erabliere_api_service.metadata["name"],
                                port=ServiceBackendPortArgs(
                                    number=erabliere_api_service.spec["ports"][0]["port"]
                                )
                            )
                        ),
                        path="/",
                        path_type="Prefix"
                    ),
                    HTTPIngressPathArgs(
                        backend=IngressBackendArgs(
                            service=IngressServiceBackendArgs(
                                name=productapi_service.metadata["name"],
                                port=ServiceBackendPortArgs(
                                    number=productapi_service.spec["ports"][0]["port"]
                                )
                            )
                        ),
                        path="/product-api",
                        path_type="Prefix"
                    )]
                )
            )]
        ),
        opts=pulumi.ResourceOptions(provider=provider, depends_on=depends_on))

    pulumi.export("erabliereapi_hostname", erabliereapi_ingress.status.apply(lambda status: status.load_balancer.ingress[0].hostname))

