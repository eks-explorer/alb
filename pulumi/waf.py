import pulumi
import requests
from pulumi_aws import wafv2, alb

def setup_WAF():
    ip = requests.get('https://api.ipify.org').content.decode('utf8')
    print(f'My public IP address is: {ip}')
    
    # WAFv2 rule for allowing access to Keycloak admin panel from a specific IP address
    keycloak_admin_ip_rule = wafv2.IpSet("keycloak_admin_ip_rule",
        addresses=[f"{ip}/32"],
        ip_address_version="IPV4",
        scope="REGIONAL",
    )

    web_acl = wafv2.WebAcl(
        "waf_protection",
        description="Rule to protect keycloak and internal service, allow other services",
        scope="REGIONAL",
        default_action={"allow": {}},
        rules=[
            wafv2.WebAclRuleArgs(
                name="keycloak_admin_rule",
                priority=0,
                action={"block": {}},
                statement=wafv2.WebAclRuleStatementArgs(
                    and_statement=wafv2.WebAclRuleStatementAndStatementArgs(
                        statements=[
                            wafv2.WebAclRuleStatementAndStatementStatementArgs(
                                not_statement=wafv2.WebAclRuleStatementAndStatementStatementNotStatementArgs(
                                    statements=[
                                        wafv2.WebAclRuleStatementAndStatementStatementNotStatementStatementArgs(
                                            ip_set_reference_statement={
                                                "arn": keycloak_admin_ip_rule.arn
                                            }
                                        )
                                    ]
                                )
                            ),
                            wafv2.WebAclRuleStatementAndStatementStatementArgs(
                                byte_match_statement={
                                    "search_string": "/admin/master",
                                    "field_to_match": {"uri_path": {}},
                                    "text_transformations": [
                                        {
                                            "priority": 0, 
                                            "type": "NONE"
                                        }
                                    ],
                                    "positional_constraint": "CONTAINS"
                                }
                            )
                        ]
                    )
                ),
                visibility_config={
                    "cloudwatch_metrics_enabled": True,
                    "metric_name": "keycloak_admin_rule",
                    "sampled_requests_enabled": True,
                },
            )
        ],
        visibility_config={
            "cloudwatch_metrics_enabled": True,
            "metric_name": "waf_protection",
            "sampled_requests_enabled": True,
        },
    )

    # Attach the waf rule to the cluster load balancer
    alb_aws = alb.get_load_balancer(
        tags={
            'elbv2.k8s.aws/cluster': 'eks-alb-pulumi',
            'ingress.k8s.aws/resource': 'LoadBalancer',
            'ingress.k8s.aws/stack': 'eks-alb-exemple-alb-group'
        }
    )

    rule_group_association = wafv2.WebAclAssociation("keycloak-rule-group-association",
        web_acl_arn=web_acl.arn,
        resource_arn=alb_aws.arn
    )

    # Export
    pulumi.export("keycloak_admin_ip_set_arn", keycloak_admin_ip_rule.arn)
    pulumi.export("load_balancer_arn", alb_aws.arn)
    pulumi.export("web_acl_arn", web_acl.arn)
    pulumi.export("rule_group_assocition_urn", rule_group_association.urn)