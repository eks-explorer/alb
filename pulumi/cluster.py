import json
import pulumi
import pulumi_awsx as awsx
import pulumi_eks as eks
import pulumi_kubernetes as k8s

def deploy_cluster_in_vpc():
    cluster_name = "eks-alb-pulumi"

    tags_eks_vpc = {
        'organisation': 'freddycoder',
        'project': 'pulumi-eks-alb',
        'environment': 'dev',
        'owner': 'freddycoder',
    }

    # Create a VPC for the cluster.
    vpc = awsx.ec2.Vpc(f"{cluster_name}-vpc",
        cidr_block="11.22.0.0/16",
        tags=tags_eks_vpc,
        subnet_specs=[
            awsx.ec2.SubnetSpecArgs(
                type=awsx.ec2.SubnetType.PRIVATE,
                tags=tags_eks_vpc.update({
                    f'kubernetes.io/cluster/{cluster_name}': 'owned',
                    'kubernetes.io/role/internal-elb': '1',
                }),
            ),
            awsx.ec2.SubnetSpecArgs(
                type=awsx.ec2.SubnetType.PUBLIC,
                tags=tags_eks_vpc.update({
                    f'kubernetes.io/cluster/{cluster_name}': 'owned',
                    'kubernetes.io/role/elb': '1',
                }),
            ),
        ])

    # Create an EKS cluster with default configuration.
    cluster = eks.Cluster(cluster_name,
        eks.ClusterArgs(
            name=cluster_name,
            vpc_id=vpc.vpc_id,
            private_subnet_ids=vpc.private_subnet_ids,
            public_subnet_ids=vpc.public_subnet_ids,
            create_oidc_provider=True,
            tags=tags_eks_vpc,
        ))

    # Create a Kubernetes provider instance that uses our cluster from above.
    provider = k8s.Provider("eks-provider", kubeconfig=cluster.kubeconfig.apply(lambda k: json.dumps(k)))

    pulumi.export("cluster_name", cluster.core.cluster.name)

    return cluster, vpc, provider