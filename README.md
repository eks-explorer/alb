# alb

An exemple on how to deploy an EKS ALB Ingress Controller with pulumi.

Run pulumi up the first time:

```
.\up.ps1
```

Pulumi destroy

```
.\destroy.ps1
```

# Documentation

## ALB

https://kubernetes-sigs.github.io/aws-load-balancer-controller/v2.4/


## AWS Immersion

https://catalog.workshops.aws/eks-immersionday/en-US/services-and-ingress/ingress

## About pulumi destroy

If the pulumi destrop command hang on deleting security group. You need to delete he network interface attached to the security group.

```
 -   ├─ aws:ec2:SecurityGroup                    eks-alb-pulumi-nodeSecurityGroup                  deleting (286s)...
```

In the aws portail in the EC2 section in the netowrk interface group you will find the network interface attached to the security group. Delete it and the pulumi destroy will continue.

